#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int numberOfLive = 5; // The player has 5 lives in the beginning.
    printf("You have %d live left\n",numberOfLive);
    printf("**** B A M ***\n"); // A big blow on his head .
    numberOfLive = 4; // Life lost
    printf("Sorry, only %d live are remaining now! \n\n",numberOfLive);
    return 0;
}
